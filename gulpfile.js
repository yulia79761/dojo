'use strict';
// запуск и использование проекта
// #home
// 0. если необходима особая сетка, сменить рабочую папку в переменной path.Project.pathProject для файла smart-grid.scss!
// 0.1.настроить сетку под себя можно в самом конце!что бы бысто попасть используй поиск #smart
// 0.2. добавить свои файлы шрифтов фонтело в /fonts/fontello/ вместе со стилями и переименовать fontello-codes.css  в fontello-codes.scss
// если что то не выходит проверить пути к шрифту в файле \src\style\libs\_fontello.scss
// 1. npm i
// 2. если необходима сетка не по умолчанию обратись к пункту 0.1 и используй команду gulp smartgrid
// 3. gulp all
// 4. в дальнейшем запуск проекта возможен по команде - gulp
// 5. (если не используеш автоматизированое разворачивание)для деплоя создай файл с переменными pass.js вида
// module.exports = {
//     host: '178.63.197.105',
//     login: 'login',
//     password: 'password',
//     globsPath: 'production/css/main.css', // какой  файл css
//     newerFolder: 'production/css/', // непрописывать папку css
//     path: '/wp-content/themes/svit-express/css/', //папка на продакшене css
//     globsPathJs: 'production/js/main.js', // какой  файл js
//     newerFolderJs: 'production/js/', // непрописывать папку js
//     pathJs: '/wp-content/themes/svit-express/js/', //папка на продакшене js
// };
// 5.1. используй команду gulp prod и после gulp ftp
// 6. папка source для исходников(макеты, шрифты)
// 7. перед сдачей проекта проверить и отключить неиспользуемые плагины и библиотеки
// как js так и scss файлы смотреть папку libs
// --------------------------------Task------------------------------------------
// gulp - task to buld and watch  testing project ('build', 'webserver', 'watch')
// gulp all - task to all build testing project ('clean', 'sprite', 'cleancache', 'build', 'watch', 'webserver')
// gulp allProd - task to all production project ('cleanProd', 'spriteProd', 'cleancacheProd', 'buildProd', 'watchProd', 'webserverProd')


// //---------IF NECESSARY, FAST ADD NEW PLUGINS TO UNCOMMENT-----
// var gulp = require('gulp'),
//     //gulp-load-plugins init
//     gp = require('gulp-load-plugins') ({
//         //need to work plugin - "del"
//         pattern: ['*', '!gulp'],
//         //need to work all this plugin
//         rename: {
//             'gulp-remove-html': 'gulpRemoveHtml',
//             'gulp-group-css-media-queries': 'gcmq',
//             'gulp-clean-css': 'cleanCSS',
//             'vinyl-ftp': 'ftp',
//             'browser-sync': 'browserSync',
//             'imagemin-pngquant': 'pngquant',
//         }
//     });

global.$ = {
    gulp: require('gulp'),
    //gulp-load-plugins init
    gp: require('gulp-load-plugins') ({
        //need to work plugin - "del"
        pattern: ['*', '!gulp'],
        //need to work all this plugin
        rename: {
            'gulp-remove-html': 'gulpRemoveHtml',
            'gulp-group-css-media-queries': 'gcmq',
            'gulp-clean-css': 'cleanCSS',
            'vinyl-ftp': 'ftp',
            'browser-sync': 'browserSync',
            'imagemin-pngquant': 'pngquant',
        },
    }),
    config: {
        configInit: require('./gulp-task/config/config.js'),
    },
    pathObject: {
        pathVar: require('./gulp-task/config/path-var.js'),
    },
    passObject: {
        passVar: require('./pass.js'),
    },
};

//CYCLE for all tasks
$.config.configInit.forEach(function (taskPath) {
    require(taskPath)();
});

//----------#BUILD FOLDER
//build task build folder TASK ---- gulp build
$.gulp.task('build', [
    'html:build',
    'js:build',
    'css:build',
    'css:buildDev',
    'fonts:build',
    'image:build'
]);

//default task to buld and watch build folder TASK ---- gulp
$.gulp.task('default', [ 'build', 'jsLibs:build', 'jsDev:build', 'watch', 'webserver']);

//task to all task build folder TASK ---- gulp all
$.gulp.task('all', ['clean', 'sprite'], function () {
    $.gulp.start('cleancache', 'build', 'jsLibs:build','jsMap:build', 'jsDev:build', 'watch', 'webserver');
});

//----------#PRODUCTION FOLDER
//build task production folder TASK ---- gulp buildProd
$.gulp.task('buildProd', [
    'html:buildProd',
    'js:buildProd',
    'css:buildProd',
    'fonts:buildProd',
    'image:buildProd',
    'image:buildProdScren'
]);

//task to all task production folder TASK ---- gulp prod
$.gulp.task('prod', ['cleanProd', 'sprite'], function () {
    $.gulp.start('buildProd', 'jsMap:buildProd', 'jsLibs:buildProd', 'webserverProd');
});

//---------#end
