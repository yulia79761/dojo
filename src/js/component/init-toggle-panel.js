    var trigger = '.js-panel-btn',
        menuToggle = '.js-panel-toggle',
        wrapParent = '.js-panel-wrapper',
        $_menuItem = $(event.target).parent(wrapParent),
        $_panel = $_menuItem.find(menuToggle);


    if ($_menuItem.data('collapsed')) {
            $_menuItem.data('collapsed', false).removeClass('is-open').addClass('is-closed');
            $_panel.slideUp();
    } else {
        //$(menuToggle).slideUp();
        //$(wrapParent).data('collapsed', false).removeClass('is-open').addClass('is-closed');
        $_menuItem.data('collapsed', true).removeClass('is-closed').addClass('is-open');
        $_panel.slideDown();
    }
