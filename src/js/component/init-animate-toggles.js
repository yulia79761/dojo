var $_clickClass     = $('.js-l-more'),
    containerClass = '.js-descript-text',
    cntText = $(containerClass).children().text();

$_clickClass.click( function(event){
    event.preventDefault();
    if ( $(this).hasClass("isDown") ) {
        $(this)
            .text('Читать подробнее')
            .siblings(containerClass).removeClass('open-text').stop().animate({height: '246px'}, 400);
    } else {
        $(this)
            .text('Скрыть')
            .siblings(containerClass).addClass('open-text').stop().animate({height: $(this).siblings(containerClass)[0].scrollHeight}, 400);
    }
    $(this).toggleClass("isDown");
    return false;
});