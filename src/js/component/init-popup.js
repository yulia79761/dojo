// Image popups
    var gImg = $('.text-style a[href*="uploads"]');
    $(gImg).magnificPopup({
        // delegate: 'a',
        type: 'image',
        removalDelay: 500,
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        },
        callbacks: {
            beforeOpen: function() {
                // just a hack that adds mfp-anim class to markup
                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        closeOnContentClick: true,
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });


    $('.popup-show').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        callbacks: {
            beforeOpen: function() {
                if ($(window).width() < 700) { this.st.focus = false; } else { this.st.focus = '#name'; }
            }
        }
    });
    $('.size-chart').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        callbacks: {
            beforeOpen: function() {
                if ($(window).width() < 700) { this.st.focus = false; } else { this.st.focus = '#name'; }
            },
            open: function() {
                //$('.slider-for').slick("getSlick").refresh();
                //$('.slider-nav-2').slick("getSlick").refresh();
                Tablesaw.init();
                // $('.slider-nav-2').on('beforeChange', function(currentSlide){
                //     Tablesaw.init();
                // });
            }
        }
    });
    $('.popup-show-woman').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        callbacks: {
            beforeOpen: function() {
                if ($(window).width() < 700) { this.st.focus = false; } else { this.st.focus = '#name'; }
            },
            open: function() {
                //$('.slider-nav-2').slick("getSlick").refresh();
                Tablesaw.init();
                // $('.slider-nav-2').on('beforeChange', function(currentSlide){
                //     Tablesaw.init();
                // });
            }
        }
    });
    $(document).on('click', '.mfp-close', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    $(document).on('click', '.popup-close', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
        location.reload();
    });
    if ($('.feedback-2').css('display') == 'block') { $('body').addClass('wrappOver'); } else { $('body').removeClass('wrappOver'); }
    $(document).on('click', '.hid-popup', function(e) {
        $('body').removeClass('wrappOver');
        $('.feedback-2').css('display', 'none');
        location.reload();
    });
    $('.close-popup').on('click', function(event) {
        event.preventDefault();
       $('.feedback-form').css({
           display: 'none'
       });
    });
