var sliders = new Array();
    var opts = {
        infinite: true,
        accessibility: false,
        arrows: true,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<div class="slide-prev slider-arrow" aria-hidden="true"><i class="icon-angle-left"></i></div>',
        nextArrow: '<div class="slide-next slider-arrow" aria-hidden="true" ><i class="icon-angle-right"></i></div>',
        responsive: [
          {
            breakpoint: 479,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          // {
          //   breakpoint: 767,
          //   settings: 'unslick'
          // }
        ]
};
var slider = $('.js-mob-slider');


slider.each(function(i, slid) {
    sliders[i] = $(slid).slick(opts);
});

$('.tabs').tabs({
    activate: function(event, ui) {
      slider.each(function(i, slid) {
      sliders[i] = $(slid).slick("getSlick").refresh();
    });
  }
});

