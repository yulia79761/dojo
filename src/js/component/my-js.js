;
function startTimer(duration, display, display_text) {
    var timer = duration,
        seconds;
    var start_text = 'секунд';
    setInterval(function() {
        seconds = parseInt(timer % 60, 10);
        seconds = seconds < 10 ? "0" + seconds : seconds;
        display.text(seconds);
        current_text = (timer == 1) ? 'секунду' :
            (timer == 4 || timer == 3 || timer == 2) ? 'секунды' :
            start_text;
        display_text.text(current_text);
        if (timer == 3) {            
            location.reload();            
        }
        if (--timer < 0) {
            timer = duration;            
            $('.feedback-form').css({display: 'none'});
        }
    }, 1000);
};

// function getTopOffset(e) {
//         var y = -55;
//         do { y += e.offsetTop; } while (e = e.offsetParent);
//         return y;
//     }
//     var block = document.getElementById('js-fixed-block');
//     if (null != block) {
//         var topPos = getTopOffset(block),
//             hBlock = $('.js-fixed-block').height(),
//             hBlock2 = $('.s-bottom').height(),
//             wBlock = $('.js-fixed-block').parents().width();

//         window.onscroll = function() {
//             var scrollHeight = Math.max(document.documentElement.scrollHeight, document.documentElement.clientHeight),
//                 blockHeight = hBlock, // высота рекламного блока 
//                 footerHeight = $('.js-footer').height(), // высота подвала 
//                 stopPos = scrollHeight - blockHeight - hBlock2  - footerHeight - 70;
//             var newcss = (topPos < window.pageYOffset) ?
//                 'top:15px; position: fixed; width:' + wBlock + 'px' : 'position:relative;';
//             if (window.pageYOffset > stopPos)
//                 newcss = 'position:static;';
//             block.setAttribute('style', newcss);
//         }
//     }

function initTogglePanel(event) {
    //= init-toggle-panel.js
};

function initResizePanel() {
    //= init-resize-panel.js
};

/*jquery-match-height
    * Website: https://github.com/liabru/jquery-match-height
    ========================================================================== 
*/
function initMatchHeight() {
    //= init-match-height.js
};

/*slick slider 
    * Website: http://kenwheeler.github.io/slick/
    ========================================================================== 
*/
function initSlider() {
    //= init-slick-slider.js
};

function initSliderNoMob() {
    //= init-slick-mob-slider.js
};
/*Popup
    * Website: http://dimsemenov.com/plugins/magnific-popup/
    ==========================================================================
*/

function funcPopup() { 
    //= init-popup.js
}

/* Scrollbar
* Website: http://brm.io/jquery-match-height/
==========================================================================
*/
function myScrollbar() {
    $('.scrollbar_inner').scrollbar();
};

function initStyler() {
    $('.style-input input').styler();
};

function initAnimateToggle() {
    //= init-animate-toggles.js
};

function initliHarmonica() {
    //= init-liHarmonica.js
};



/*jQuery ready
    ========================================================================== 
*/
jQuery(document).ready(function($) {
    'use strict';

  var searcHeader = $(".header .btn-search");
  var searcInHeader = $(".header .search-in");
  searcHeader.click(function(e) {
      $(this).toggleClass("on");
      if (searcInHeader.css('display') == 'none') {
          searcInHeader.fadeIn(200);
      } else {
          searcInHeader.fadeOut(200);
      }
      e.preventDefault();
  });

//---------------init-toggle-panel
    $('.js-panel-btn').on('click', function(event) {
        initTogglePanel(event);
    });

    $('.sub-menu li a').hover(function() {
        $(this).parent().addClass('js-activ-item').siblings('li').removeClass('js-activ-item');
    }, function() {
        $('.sub-menu li:first a').addClass('js-activ-item');
    });
    $('#nav .sub-menu').each(function() {
        if ($(this).children('li').hasClass('sub-menu_cnt')) {
            $(this).addClass('sub-menu_content-wrp');
        } else {
            $(this).addClass('no-content-wrp');
        }
    });

    $(".js-accordion-menu .cat-item").each(function(index, el) {
        if ($(this).hasClass('current-cat')) {
            //console.log($(this))
            $(this).parents("ul").css({display:"block"}).siblings('.open').addClass('harOpen');
            $(this).children('ul').css({display:"block"}).siblings('.open').addClass('harOpen');
        }
    });
    $(".nav-aside_box:first .product-categories").css({display:"block"}).siblings('.open').addClass('harOpen');
    $(".filtr-product-categories").css({display:"block"}).siblings('.open').addClass('harOpen');
    $(".js-accordion-menu .nav-aside_checked li").each(function(index, el) {
        if ($(this).hasClass('chosen')) {
            //console.log($(this))
            $(this).parents("ul").css({display:"block"}).siblings('a').addClass('harOpen');
            //$(this).children('ul').css({display:"block"});
        }
     });

    var top_show = 400;
    var delay = 600;
    $(window).scroll(function() {
        if ($(this).scrollTop() > top_show) $('#top').fadeIn();
        else $('#top').fadeOut();
    });
    $('#top').click(function() { $('body, html').animate({ scrollTop: 0 }, delay); });

function funcFormCity() {
        var selection_textCity = $('#billing_delivery_state option:selected').val();
        switch(selection_textCity) {
            case 'kharkiv':
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').show();
               $('#billing_delivery_salon_od_field').hide();
            break;
            case 'kyiv':
               $('#billing_delivery_salon_kiev_field').show();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').hide();
            break;
            case 'odessa':
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').show();
            break;
            default:
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').hide();
            break;
        };

    };

 function funcForm() {
        var selection_text = $('.form-row select option:selected').val();
        switch(selection_text) { 
            case 'nova_poshta':
               $('#billing_state_field').show();
               $('#billing_country_russia_field').hide();
               $('#billing_delivery_state_field').hide();
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').hide();
               $('#np-map').show();
               $('#billing_city_field').show();
               $('#billing_address_1_field').show();
               $('#billing_address_2_field').hide();
            break;
            case 'mistexpress':
               $('#billing_state_field').show();
               $('#billing_country_russia_field').hide();
               $('#billing_delivery_state_field').hide();
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').hide();
               $('#billing_city_field').show();
               $('#billing_address_1_field').show();
               $('#np-map').hide();
               $('#billing_address_2_field').hide();
            break;
            case 'russia':
                $('#billing_state_field').hide();
               $('#billing_country_russia_field').show();
               $('#billing_delivery_state_field').hide();
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').hide();
               $('#billing_city_field').show();
               $('#billing_address_1_field').hide();
               $('#np-map').hide();
               $('#billing_address_2_field').show();
            break;
            case 'samovivoz':
                $('#billing_state_field').hide();
               $('#billing_country_russia_field').hide();
               $('#billing_delivery_state_field').show();
               $('#billing_city_field').hide();
               $('#billing_address_1_field').hide();
               $('#np-map').hide();
               $('#billing_address_2_field').hide();
            break;
            case 'kurierom':
               $('#billing_state_field').hide();
               $('#billing_city_field').hide();
               $('#billing_address_1_field').hide();
               $('#billing_country_russia_field').hide();
               $('#billing_delivery_state_field').hide();
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').hide();
               $('#np-map').hide();
               $('#billing_address_2_field').show();
            break;
            default:
               $('#billing_state_field').hide();
               $('#billing_city_field').hide();
               $('#billing_address_1_field').hide();
               $('#billing_country_russia_field').hide();
               $('#billing_delivery_state_field').hide();
               $('#billing_delivery_salon_kiev_field').hide();
               $('#billing_delivery_salon_kh_field').hide();
               $('#billing_delivery_salon_od_field').hide();
               $('#np-map').hide();
               $('#billing_address_2_field').hide();
            break;
        }
    }; setTimeout(funcForm, 500);

    function funcFormReset() {
        $('#billing_country_russia_field input').val('');
        $('#billing_state_field input').val('');
        $('#billing_city_field input').val('');
        $('#billing_address_1_field input').val('');
        $('#billing_address_2_field input').val('');
        $('#billing_delivery_state').val('Выбрать');
        $('#billing_delivery_state_field').removeClass('woocommerce-validated');
        $('#billing_country_russia_field').removeClass('woocommerce-validated');
        $('#billing_state_field').removeClass('woocommerce-validated');
        $('#billing_city_field').removeClass('woocommerce-validated');
        $('#billing_address_1_field').removeClass('woocommerce-validated');
        $('#billing_address_2_field').removeClass('woocommerce-validated');
    }
    $('#billing_delivery').on('change', function(){
        setTimeout(funcForm, 500); 
        setTimeout(funcFormReset, 500);
    });
    $('#billing_delivery_state').on('change', function(){
        setTimeout(funcFormCity, 500);
        $('#billing_delivery_salon_kh').val('value','Выбрать');
        $('#billing_delivery_salon_kiev').val('value','Выбрать');
        $('#billing_delivery_salon_od').val('value','Выбрать');
   });
   $('#npw-cities').on('change', function(){
        $(this).parent("#npw-map-state-list").addClass('sel-val-rem');
   });
   $('#npw-map-sidebar-ul').on('click', 'li', function(event) {
       var npwCity = $(this).children('.npw-list-city').text(),
           npwWarehouse = $(this).children('.npw-list-warehouse').text(),
           npwAddress = $(this).children('.npw-list-address').text();
       $('#billing_address_1').val(npwWarehouse + ' / ' + npwAddress );
       $('#billing_city').val(npwCity);
   });
    // $(".price_slider.slider-range" ).slider({
    //     stop: function(event, ui) {
    //         $('.form-price-range').submit();
    //     }
    // });
    
    setTimeout(initMatchHeight, 2000);
    initSlider();
    initSliderNoMob();
    myScrollbar();
    funcPopup();
    initStyler();
    initAnimateToggle();
    initliHarmonica();
});

/*jQuery resize
    ========================================================================== 
*/
jQuery(window).on('resize orientationchange', function($) { 
    initResizePanel();
});

/*Ajax stop
    ========================================================================== 
*/
jQuery(document).ajaxStop(function($) {
    setTimeout(initMatchHeight, 2000);
    initStyler();
});