$('.js-one-slider').slick({
    dots: true,
    fade: true,
    slidesToShow: 1,
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 5000,
    prevArrow: '<div class="slide-prev slider-arrow" aria-hidden="true"><i class="icon-angle-left"></i></div>',
    nextArrow: '<div class="slide-next slider-arrow" aria-hidden="true" ><i class="icon-angle-right"></i></div>',
    responsive: [{
        breakpoint: 480,
        settings: {
            arrows: false
        }
    }]
});

$('.js-nav-slider').slick({
    dots: false,
    fade: false,
    infinite: false,
    slidesToShow: 5,
    adaptiveHeight: true,
    prevArrow: '<div class="slide-prev slider-arrow" aria-hidden="true"><i class="icon-angle-left"></i></div>',
    nextArrow: '<div class="slide-next slider-arrow" aria-hidden="true" ><i class="icon-angle-right"></i></div>',
    responsive: [{
        breakpoint: 768,
        settings: {
            slidesToShow: 4,
        }
    },{
        breakpoint: 480,
        settings: {
            slidesToShow: 3
        }
    }]
});

$('.slick-slide:first .yith_magnifier_thumbnail').addClass('img-current');
$('.yith_magnifier_thumbnail').click(function() {
    $('.yith_magnifier_thumbnail').removeClass('img-current');
    $(this).addClass('img-current');
});

$('.js-three-slider').slick({
    dots: false,
    fade: false,
    slidesToShow: 3,
    adaptiveHeight: true,
    prevArrow: '<div class="slide-prev slider-arrow" aria-hidden="true"><i class="icon-angle-left"></i></div>',
    nextArrow: '<div class="slide-next slider-arrow" aria-hidden="true" ><i class="icon-angle-right"></i></div>',
    responsive: [{
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
        }
    },{
        breakpoint: 480,
        settings: {
            slidesToShow: 1
        }
    }]
});

$('.js-four-slider').slick({
    dots: false,
    fade: false,
    slidesToShow: 4,
    adaptiveHeight: true,
    prevArrow: '<div class="slide-prev slider-arrow" aria-hidden="true"><i class="icon-angle-left"></i></div>',
    nextArrow: '<div class="slide-next slider-arrow" aria-hidden="true" ><i class="icon-angle-right"></i></div>',
    responsive: [{
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
        }
    },{
        breakpoint: 480,
        settings: {
            slidesToShow: 1
        }
    }]
});

$('.js-slider-def').slick({
    dots: false,
    slidesToShow: 6,
    prevArrow: '<div class="slide-prev slider-arrow" aria-hidden="true"><i class="icon-angle-left"></i></div>',
    nextArrow: '<div class="slide-next slider-arrow" aria-hidden="true" ><i class="icon-angle-right"></i></div>',
    responsive: [{
        breakpoint: 768,
        settings: {
            slidesToShow: 4,
        }
    },{
        breakpoint: 480,
        settings: {
            slidesToShow: 3,
        }
    },{
        breakpoint: 320,
        settings: {
            slidesToShow: 2
        }
    }]
});

$('.slider-nav-2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    //fade: true,
    adaptiveHeight: true,
    prevArrow: '<div class="icon-angle-left slick-arrow-left" aria-hidden="true"></div>',
    nextArrow: '<div class="icon-angle-right slick-arrow-right" aria-hidden="true" ></div>'
});

$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    //fade: true,
    adaptiveHeight: true,
    prevArrow: '<div class="icon-angle-left slick-arrow-left" aria-hidden="true"></div>',
    nextArrow: '<div class="icon-angle-right slick-arrow-right" aria-hidden="true" ></div>',
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 1,
    arrows: false,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    adaptiveHeight: true,
    //centerMode: true,
    focusOnSelect: true
});