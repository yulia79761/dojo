function resizeblockCollapsed() {
    var $_auxiliaryWrp = $('.resizeWrp'),
        $_auxiliaryPanel = $('.resizePanel');
    if ($(window).width() >= 768) {
        $_auxiliaryWrp.removeClass('js-closed').removeClass('js-open')
        $_auxiliaryPanel.removeAttr('style')
    }
}