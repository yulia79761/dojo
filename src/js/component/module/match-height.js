/* jquery-match-height
* Website: https://github.com/liabru/jquery-match-height*/
function funcMatchHeight() {
    $('.match-height').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
    console.log("TEST JS");
};