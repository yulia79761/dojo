//module starttimer 
function startTimer(duration, display, display_text) {
    var timer = duration,
        seconds;
    var start_text = 'секунд';
    setInterval(function() {
        seconds = parseInt(timer % 60, 10);
        seconds = seconds < 10 ? "0" + seconds : seconds;
        display.text(seconds);
        current_text = (timer == 1) ? 'секунду' :
            (timer == 4 || timer == 3 || timer == 2) ? 'секунды' :
            start_text;
        display_text.text(current_text);
        if (--timer < 0) {
            timer = duration;
            location.reload();
        }
    }, 1000);
};