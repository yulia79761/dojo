module.exports = function() {
    //----------#BUILD FOLDER
    //main css build version project
    $.gulp.task('css:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.style)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.sass())
            .pipe($.gp.autoprefixer({
                browsers: ['last 10 versions'],
                cascade: false
            }))
            .pipe($.gp.sourcemaps.write())
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.css))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });

    // dev css build version project
    $.gulp.task('css:buildDev', function() {
        $.gulp.src($.pathObject.pathVar.path.src.styleDev)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.sass())
            .pipe($.gp.autoprefixer({
                browsers: ['last 10 versions'],
                cascade: false
            }))
            .pipe($.gp.sourcemaps.write())
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.css))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });

    //----------#PRODUCTION FOLDER
    //main css production version project
    $.gulp.task('css:buildProd', function() {
        $.gulp.src($.pathObject.pathVar.path.src.style)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.sass())
            .pipe($.gp.autoprefixer({
                browsers: ['last 16 versions'],
                cascade: false
            }))
            .pipe($.gp.gcmq())
            .pipe($.gp.cleanCSS({debug: true}, function(details) {
                console.log(details.name + ': ' + details.stats.originalSize);
                console.log(details.name + ': ' + details.stats.minifiedSize);
            }))
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.css))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });
};