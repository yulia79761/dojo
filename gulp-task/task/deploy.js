module.exports = function() {
    // task for deploy on production
    // незабудь заменить данные для входа в файле pass.js
    $.gulp.task('ftp-task', function() {
        var conn = $.gp.ftp.create({
            host: $.passObject.passVar.host,
            user: $.passObject.passVar.login,
            password: $.passObject.passVar.password,
        });
        return $.gulp.src($.passObject.passVar.globsPath)
            .pipe(conn.newer($.passObject.passVar.newerFolder))
            .pipe(conn.dest($.passObject.passVar.path));

    });
    // task for deploy on production wiht time out
    $.gulp.task('ftp', ['css:buildProd'], function(cb) {
        setTimeout(function() {
            $.gulp.start('ftp-task');
            cb();
            $.gp.notify("FTP file was UPLOAD!").write('');
        }, 2000);
    });

    // task for deploy on production js
    // незабудь заменить данные для входа в файле pass.js
    $.gulp.task('ftp-task-js', function() {
        var conn = $.gp.ftp.create({
            host: $.passObject.passVar.host,
            user: $.passObject.passVar.login,
            password: $.passObject.passVar.password,
        });
        return $.gulp.src($.passObject.passVar.globsPathJs)
            .pipe(conn.newer($.passObject.passVar.newerFolderJs))
            .pipe(conn.dest($.passObject.passVar.pathJs));

    });
    // task for deploy on production wiht time out
    $.gulp.task('ftp-js', ['js:buildProd'], function(cb) {
        setTimeout(function() {
            $.gulp.start('ftp-task-js');
            cb();
            $.gp.notify("FTP JS file was UPLOAD!").write('');
        }, 2000);
    });
};