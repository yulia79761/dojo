module.exports = function() {
    //----------#BUILD FOLDER
    //html file build version
    $.gulp.task('html:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.html)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.rigger())
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.html))
            .on('end', $.gp.browserSync.reload);
    });

    //----------#PRODUCTION FOLDER
    //html file production version
    $.gulp.task('html:buildProd', function() {
        $.gulp.src($.pathObject.pathVar.path.src.htmlProd)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.rigger())
            .pipe($.gp.gulpRemoveHtml())
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.html))
            .on('end', $.gp.browserSync.reload);
    });
};