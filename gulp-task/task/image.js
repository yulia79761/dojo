module.exports = function() {
    //sprite
    $.gulp.task('sprite', function generateSpritesheets() {
        var spriteData = $.gulp.src($.pathObject.pathVar.path.src.spriteOrigin)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.spritesmith({
                // ------ to support the retina uncommented bottom line (add images twice larger and prefiks ...@2x.png)
                //retinaSrcFilter: [$.pathObject.pathVar.path.src.spriteretinaSrcFilter],
                //retinaImgName: $.pathObject.pathVar.path.src.spriteimgNameRet,
                imgName: $.pathObject.pathVar.path.src.spriteimgName,
                cssName: 'sprite.scss',
                algorithm: 'binary-tree',
                padding: 5,
            }));
        spriteData.img.pipe($.gulp.dest($.pathObject.pathVar.path.src.spriteImgPath));
        spriteData.css.pipe($.gulp.dest($.pathObject.pathVar.path.src.spriteSass));
    });


    //----------#BUILD FOLDER
    //image optimize build version project
    $.gulp.task('image:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.img)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.cache($.gp.imagemin({
                progressive: true,
                svgoPlugins: [{ removeViewBox: false }],
                use: $.gp.pngquant(),
                interlaced: true
            })))
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.img))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });

    //----------#PRODUCTION FOLDER
    //image optimize production version project
    $.gulp.task('image:buildProd', function() {
        $.gulp.src($.pathObject.pathVar.path.src.imgProd)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gp.imagemin({
                progressive: true,
                svgoPlugins: [{ removeViewBox: false }],
                use: $.gp.pngquant(),
                interlaced: true
            }))
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.img))
            .pipe($.gp.browserSync.reload({ stream: true }));
    });
    //image screenshot optimize production version project
    $.gulp.task('image:buildProdScren', function() {
        $.gulp.src($.pathObject.pathVar.path.src.imgScren)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.imgScren))
    });
};