module.exports = function() {
    //----------#BUILD FOLDER
    //delet folder build version project
    $.gulp.task('clean', function() {
        return $.gp.del.sync($.pathObject.pathVar.path.clean);
    });

    //clearcache all build version project
    $.gulp.task('cleancache', function() {
        return $.gp.cache.clearAll();
    });

    //fonts file build version project
    $.gulp.task('fonts:build', function() {
        $.gulp.src($.pathObject.pathVar.path.src.fonts)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gulp.dest($.pathObject.pathVar.path.build.fonts))
    });

    //----------#PRODUCTION FOLDER
    //delet folder production version project
    $.gulp.task('cleanProd', function() {
        return $.gp.del.sync($.pathObject.pathVar.path.cleanProd);
    });

    //fonts file production version project
    $.gulp.task('fonts:buildProd', function() {
        $.gulp.src($.pathObject.pathVar.path.src.fonts)
            .pipe($.gp.plumber({ errorHandler: $.gp.notify.onError("Error: <%= error.message %>") }))
            .pipe($.gulp.dest($.pathObject.pathVar.path.production.fonts))
    });

};